import requests
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-t','--target',help="Objetivo")
parser = parser.parse_args()

def main():
  if parser.target:
    try:
      objrequest = requests.get(url=parser.target)
      header = dict(objrequest.headers)
      for h in header:
        print(h+" : "+header[h])
    except:
      print('No es posible conectarse a esta web')
  else:
    print("Debes indicar la URL. Si necesitas ayuda debes de pasar el argumento --help o -h")

if __name__ == '__main__':
  main()
